#!/bin/bash

curl -s http://www.restaurangljunggren.se/#lunch| \
	sed -e 's/<[^>]*>//g;'| \
	sed -e 's/^[ \t]*//'| \
	grep -i -A1 "${today}"| \
	egrep -v "^$|${today}"| \
	recode html..utf-8
