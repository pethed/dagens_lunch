ABOUT
=====

Dockerfile that creates an image with todays lunch menu with examples from around Medborgarplatsen in stockholm.



USAGE
-----

Copy the Dockerfile to desired folder on your computer.

From that folder, run 
  
 docker build -t lunch:1 .
    
to build the image. After build completes run

 docker run --rm -i lunch:1 
      
to view the menu.

To make things prettier add the following to your .bashrc
      
 alias lunch='docker run --rm lunch:1'
 
 
UPDATE: Added a ready-build image to GitLab registry, so just type in 

 docker run --rm registry.gitlab.com/pethed/dagens_lunch
to get the result.

